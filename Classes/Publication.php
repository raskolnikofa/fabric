<?php

class Publication{

    public $id;
    public $name;
    public $text;
    public static $type;

    public function __construct($id, $name, $text){
        $this->name = $name;
        $this->text = $text;
        $this->id = $id;
    }

   public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }

    public function getText(){
        return $this->text;
    }

    public function getTotalInfo(){
        echo '<b>'.$this->getName().'</b>'.'<br>'.$this->getText();
    }

    public static function getInstance( $id, PDO $pdo ){
        
        $sql = " SELECT * FROM publication WHERE id = :id ";
        $pr_sql = $pdo->prepare($sql);
        $pr_sql->bindValue(':id', $id);
        $pr_sql->execute();

        $obj = $pr_sql->fetchObject();

        switch($obj->type){
            case 'news':
                $publication = new News(
                    $obj->id,
                    $obj->name,
                    $obj->text,
                    $obj->source,
                    $obj->type
                );
                break;
            case 'article':
                $publication = new Article(
                    $obj->id,
                    $obj->name,
                    $obj->text,
                    $obj->author,
                    $obj->type
                );
                break;
            default:
                return NULL;
                break;

        }

        $publication->setId($id);
        return $publication;

    }
}