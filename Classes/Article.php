<?php
class Article extends Publication{
    public $author;

    public function __construct($id, $name, $text, $author){
        parent::__construct($id, $name, $text);
        $this->author = $author;}

    public function getShortPreview(){
        return parent::getShortPreview().' Author: '.$this->author;
    }

    public function getTotalInfo(){
        echo parent::getTotalInfo().'<br>'.' Author: '.$this->author;}
}