<?php
class PublicationWriter{
    public $publications = array();

    public function __construct(PDO $pdo, $type){
        
        $sql = " SELECT * FROM publication WHERE type = :type ";
        $pr_sql = $pdo->prepare($sql);
        $pr_sql->bindValue(':type', $type);
        $pr_sql->execute();


        $obj = $pr_sql->fetchAll();

            foreach ($obj as $key => $value) {

           switch($value['type']){

            case 'news':
                $this->publications[] = new News(
                    $value['id'],
                    $value['name'],
                    $value['text'],
                    $value['source']
                );
                break;
        
            case 'article':
                $this->publications[] = new Article(
                    $value['id'],
                    $value['name'],
                    $value['text'],
                    $value['author']
                );
                break;

            default:
            $this->publications[] = 'Mistake';
                break;
        }
}
    }



    public function getPublications(){
       foreach ($this->publications as $key=>$value) {
             echo $value->name.'<a href= "second_page.php?id= '.$value->id.'  ">Read All</a>'.'<br>';
        }
    }
}
