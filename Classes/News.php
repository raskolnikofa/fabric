<?php
class News extends Publication{
    public $source;
    
    public function __construct($id, $name, $text, $source){
        parent::__construct($id, $name, $text);
        $this->source = $source;}

    public function getShortPreview(){
        return parent::getShortPreview().' Source: '.$this->source;
    }

    public function getTotalInfo(){
        echo parent::getTotalInfo(). '<br>' .' Source: '.$this->source;}
}