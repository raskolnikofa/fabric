-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 25 2017 г., 19:26
-- Версия сервера: 5.7.19-0ubuntu0.16.04.1
-- Версия PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `publication`
--
CREATE DATABASE IF NOT EXISTS `publication` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `publication`;

-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `text` text NOT NULL,
  `type` text NOT NULL,
  `author` text NOT NULL,
  `source` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `publication`
--

INSERT INTO `publication` (`id`, `name`, `text`, `type`, `author`, `source`) VALUES
(1, 'Tesco criticised for deducting £3.4m from plastic bag tax charity donations', 'Millions of pounds in administration costs were deducted from the charitable donations made by Tesco using funds generated from the plastic bag tax, government data has revealed. No other major supermarket made any such deductions, leading senior MPs to urge Tesco to follow their lead.\r\n\r\nThe 5p charge for plastic bags was introduced in England in October 2015 and has led to an 83% reduction in their use, equivalent to 9bn fewer bags. It is also credited with a drop by nearly half in plastic bags found littering beaches.\r\n\r\nThe government is clear that it expects shops to donate the proceeds of the 5p plastic bag tax to good causes. The latest official statistics, covering the year to March 2017, show that Tesco sold 637m carrier bags, raising £31.9m in proceeds. But the supermarket giant deducted £3.4m to cover the “cost of administering donations”, equivalent to more than 10% of the total.Tesco topped the list of plastic bags sales but no other company in the top 10 made administration deductions, including Asda, Morrison, the Co-op, Marks and Spencer, Aldi, Iceland and Waitrose.\r\n\r\n“The legislation for the 5p plastic bag charge is clear that the money raised should go to good causes,” said Mary Creagh MP, chair of the environmental audit committee. “Five years after the horsemeat scandal and three years after a false accounting scandal, Tesco finds itself again in the spotlight for doing the wrong thing. They should drop this ridiculous charge immediately.”\r\n\r\nNeil Parish MP, chair of the environment, food and rural affairs select committee, said: “As much money as possible from the plastic bag tax should be going to charitable causes. It would be great to see Tesco follow the lead of other retailers and not deduct admin costs. That would be a very positive step for Britain’s biggest supermarket to take.”\r\n\r\nA spokesman for the Department for Environment, Food and Rural Affairs said: “There is clear expectation to donate the proceeds of the plastic bag charge to good causes. We know that £95m raised has been donated to good causes so far and by publishing the data on donations, along with the number of bags distributed, this has the added effect of encouraging retailers to donate to good causes.”Tesco’s spokesman said: “Since launching in 2015, our Bags of Help initiative has provided more than £33m to over 6,400 local community projects. A small proportion of the money raised is used to run and administer the scheme in partnership with the charity Groundwork, who help distribute the money to good causes.”\r\n\r\nCompanies can deduct a portion of the revenue to cover “reasonable costs” of administering the donations. The spokesman said Tesco’s administration costs included customer communications and the provision of voting tokens and booths which customers use to choose the charities that are supported. The spokesman said Tesco did not profit from money retained for administration.\r\n\r\nTesco stopped selling 5p “single-use” plastic bags on 28 August, instead offering a “reusable” 10p bag, which will be replaced free of charge if damaged. The company said the proceeds of the 10p bag sales would continue to fund community projects.', 'article', 'John Week', ''),
(2, 'Irma\'s destruction: island by island', 'Antigua and Barbuda\r\nBarbuda, the first island to feel the force of Hurricane Irma was devastated by its high winds, with Gaston Browne, prime minister of Antigua and Barbuda, saying 90% of buildings had been destroyed and 60% of the population of around 1,400 people left homeless.\r\n\r\nOne person – a two-year-old child – is confirmed to have died in the storm. Michael Joseph, president of the Red Cross in Antigua and Barbuda said:\r\n\r\nThe devastation is not like we’ve ever seen before – we’re talking about the whole country … of Barbuda being significantly destroyed.\r\n\r\nCritical facilities including roads and communications systems were ravaged, with the recovery effort set to take months or years. Some residents are expected to be evacuated to the larger sister island of Antigua – where damage was less severe – as part of relief efforts and ahead of the prospective arrival of Hurricane Jose this weekend.Anguilla\r\nOne person died in the British overseas territory, said Ronald Jackson, executive director of the Caribbean disaster and emergency management agency, who added that “police stations, hospitals, school facilities, three or four emergency shelters, a home for the infirm and the aged, as well as the fire station”, along with many homes, had been damaged or destroyed.\r\n\r\nLive Hurricane Irma: Islands suffer huge damage as storm heads for Dominican Republic and Haiti – latest\r\nMost powerful hurricane ever recorded over Atlantic Ocean batters Barbuda, St Martin and Puerto Rico as it moves west with category 5 winds and rains\r\n Read more\r\nThe tourist board said major resorts on the island had withstood the onslaught. The airport and two ports remain closed.\r\n\r\nThe British government has been accused of a failure to respond speedily to the devastation.\r\n\r\nSt Kitts & Nevis\r\nPrime minister Timothy Harris said St Kitts was “spared the full brunt” of Irma, but warned of “significant damage” to property and infrastructure, as well as power failures. The airport is due to reopen on Thursday.', 'article', 'Ashley Thomas', ''),
(3, 'North Korea crisis: US seeks Kim Jong-un asset freeze', 'The US has proposed a range of new United Nations sanctions against North Korea, including an oil ban and a freeze on leader Kim Jong-un\'s assets.\r\nThe draft resolution circulated to the Security Council members comes after North Korea\'s sixth nuclear test and repeated missile launches.\r\nPyongyang also claims to have developed a hydrogen bomb and continues to threaten to strike the US.\r\nChina and Russia are both expected to oppose further sanctions.\r\nNorth Korea crisis in 300 words\r\nHow should Trump handle North Korea?\r\nHow do you defend against North Korea?\r\nNorth Korea is already under highly restrictive sanctions imposed by the UN that are intended to force the leadership to curtail its weapons programmes.', 'news', '', 'The Guardian'),
(4, 'Why is Bulgaria\'s population falling off a cliff?', 'Bulgaria is projected to have the fastest-shrinking population in the world. It\'s already lost a fifth of its population since the 1990s. But what does this mean for those who remain?\r\nDeep in the Bulgarian countryside, in the western province of Pernik, I make a rare discovery.\r\nIt\'s not Stoyan Evtimov\'s traditional embroidered woollen tunic that makes him unusual.\r\nIt\'s the fact he\'s a thirty-something living in a village. "All my friends that I grew up with here left long ago," he says.\r\nLike many young Bulgarians, they moved to towns and cities in search of work.\r\nStoyan considers himself lucky to have employment in the mountain village of Peshtera, leading its folk-singing group and organising an annual music festival in an attempt to revive traditional marriage music, and the village.\r\nEven so, he is finding village life unsustainable.', 'news', '', 'BBC');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
