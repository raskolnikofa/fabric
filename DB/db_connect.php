<?php
try
{
    $pdo = new PDO('mysql:host=localhost;dbname=publication',
        'root','1234');
    $pdo->exec('SET NAMES "utf8"');
    $pdo->setAttribute(PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION);
}catch (Exception $e)
{
    echo 'Mistake:'.$e->getMessage();
}
