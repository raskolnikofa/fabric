<?php

require_once('DB/db_connect.php');
require_once('Classes/Publication.php');
require_once('Classes/News.php');
require_once('Classes/Article.php');
require_once('Classes/PublicationWriter.php');

echo "<b>News</b><hr>";

$qwerty = new PublicationWriter($pdo, 'news');
$qwerty->getPublications();

echo "<br>";

echo "<b>Article</b><hr>";

$qwerty = new PublicationWriter($pdo, 'article');
$qwerty->getPublications();